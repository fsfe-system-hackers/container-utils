#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2022 Free Software Foundation Europe e.V. <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# Imports from standard library
import os
from typing import Sequence

# Import packages
import requests

# Import utility functions
from container_utils.console import console

# Define API constants
BASE_URL = os.environ.get("DRONE_SERVER", "https://drone.fsfe.org")
API_URL = BASE_URL + "/api"

try:
    AUTH = {"Authorization": "Bearer " + str(os.getenv("DRONE_TOKEN"))}
except KeyError as e:  # pragma: no cover
    console.log("No Drone token found in environment variable. Please check.")
    raise e

# https://readme.drone.io/api/repos/repo_list/
def drone_active_repos() -> Sequence[list]:
    """Returns a list of all repos which are registered to Drone."""
    try:
        url = API_URL + "/user/repos"
        response = requests.get(url, headers=AUTH).json()
        if response:
            return [repo for repo in response if repo["active"] == True]
        else:
            console.log("No drone repos parsed. Check you config.cfg file")
            raise ValueError
    except requests.exceptions.RequestException as e:  # pragma: no cover
        console.log("Error when calling drone_active_repos()")
        console.print_exception()
        raise e


# https://docs.drone.io/api/user/user_sync/
def drone_sync() -> int:
    """Synchronize the currently authenticated user's repository list."""
    try:
        url = API_URL + "/user/repos"
        response = requests.post(url, headers=AUTH).json()
        if response != None:
            console.log("Successfully synced repository list.")
            return 0
        else:
            console.log("This didn't work. Check your setup.")
            raise ValueError
    except requests.exceptions.RequestException as e:  # pragma: no cover
        console.log("Error when calling drone_sync().")
        console.print_exception()
        raise e


# https://docs.drone.io/api/repos/
def drone_repo_is_active(owner_name: str, repo_name: str) -> bool:
    """Check whether repo is an active drone repository."""
    try:
        url = API_URL + "/repos/" + owner_name + "/" + repo_name
        repo = requests.get(url, headers=AUTH).json()
        if repo["active"] == True:
            return True
        else:
            return False
    except requests.exceptions.RequestException as e:  # pragma: no cover
        console.log("Error when calling drone_repo_is_active().")
        console.print_exception()
        raise e


# https://docs.drone.io/api/builds/build_list/
def drone_all_builds(owner_name: str, repo_name: str) -> Sequence[list]:
    """Get a list of all builds for a repository."""
    url = API_URL + "/repos/" + owner_name + "/" + repo_name + "/builds"
    try:
        all_builds = requests.get(url, headers=AUTH).json()
        return all_builds
    except requests.exceptions.RequestException as e:  # pragma: no cover
        console.log("Error when calling drone_all_builds()")
        console.print_exception()
        raise e


# https://docs.drone.io/api/builds/build_list/
def drone_last_build_number(
    owner_name: str, repo_name: str, target_branches: list = ["master", "main"], event: str = "push"
) -> int:
    """Get the number for the last build for a given branch and for a specific event."""
    try:
        all_builds = drone_all_builds(owner_name=owner_name, repo_name=repo_name)
        last_id = max(
            [
                build["number"]
                for build in all_builds
                if build["target"] in target_branches and build["event"] == event
            ],
            default=0,
        )
        return int(last_id)
    except Exception as e:  # pragma: no cover
        console.log("Error when calling drone_last_build_number()")
        console.print_exception()
        raise e


# https://docs.drone.io/api/builds/build_start/
def drone_restart_build(owner_name: str, repo_name: str, build_number: int) -> dict:
    """Restart drone build by build number and return build info."""
    url = (
        API_URL
        + "/repos/"
        + owner_name
        + "/"
        + repo_name
        + "/builds/"
        + str(build_number)
    )
    try:
        build = requests.post(url, headers=AUTH).json()
        return dict(build)
    except requests.exceptions.RequestException as e:  # pragma: no cover
        console.log("Error when calling drone_restart_build()")
        console.print_exception()
        raise e
