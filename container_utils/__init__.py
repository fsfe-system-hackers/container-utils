#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2022 Free Software Foundation Europe e.V. <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

#
from rich.traceback import install

install(show_locals=True)  # traceback
