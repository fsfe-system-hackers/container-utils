#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2022 Free Software Foundation Europe e.V. <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from container_utils.drone import (
    drone_active_repos,
    drone_repo_is_active,
    drone_sync,
    drone_all_builds,
    drone_last_build_number,
    drone_restart_build,
)


def test_drone_active_repos():
    repos = drone_active_repos()
    assert len(repos) > 10


def test_drone_sync():
    assert drone_sync() == 0


def test_drone_is_active():
    # Testing a public active repo
    assert drone_repo_is_active("fsfe-system-hackers", "lychee") == True
    # Testing a private active repo
    assert drone_repo_is_active("fsfe-system-hackers", "fsfe-cd") == True
    # Testing an inactive repo
    assert drone_repo_is_active("FSFE", "internal") == False


def test_drone_all_builds():
    # Testing a public active repo
    assert type(drone_all_builds("fsfe-system-hackers", "lychee")) == list
    assert len(drone_all_builds("fsfe-system-hackers", "lychee")) >= 1
    # Testing a private active repo
    assert type(drone_all_builds("fsfe-system-hackers", "fsfe-cd")) == list
    assert len(drone_all_builds("fsfe-system-hackers", "fsfe-cd")) >= 1
    # Testing an inactive repo
    assert type(drone_all_builds("FSFE", "internal")) == list
    assert len(drone_all_builds("FSFE", "internal")) == 0


def test_drone_last_build_number():
    # Testing a public active repo
    assert type(drone_last_build_number("fsfe-system-hackers", "lychee")) == int
    assert drone_last_build_number("fsfe-system-hackers", "lychee") > 0
    # Testing a private active repo
    assert type(drone_last_build_number("fsfe-system-hackers", "fsfe-cd")) == int
    assert drone_last_build_number("fsfe-system-hackers", "fsfe-cd") > 0
    # Testing an inactive repo
    assert type(drone_last_build_number("FSFE", "internal")) == int
    assert drone_last_build_number("FSFE", "internal") == 0
    # Test the passing of a specific branch and event
    assert (
        type(
            drone_last_build_number(
                "fsfe-system-hackers",
                "lychee",
                "renovate/configure",
                "push",
            )
        )
        == int
    )
    assert (
        drone_last_build_number(
            "fsfe-system-hackers",
            "mailman-settings-monitor",
            "check_mod_q",
            "push",
        )
        == 41
    )


def test_drone_restart_build():
    old_build_number = drone_last_build_number("fsfe-system-hackers", "lychee")
    assert (
        type(drone_restart_build("fsfe-system-hackers", "lychee", old_build_number))
        == dict
    )
    new_build_number = drone_last_build_number("fsfe-system-hackers", "lychee")
    assert new_build_number > old_build_number
