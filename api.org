#+TITLE: API

#+begin_src restclient
POST https://drone.fsfe.org/api/repos/fsfe-system-hackers/lychee/builds/39
Authorization: Bearer ***
#+end_src

#+RESULTS:
#+BEGIN_SRC js
{
  "id": 64510,
  "repo_id": 10084,
  "trigger": "linus",
  "number": 45,
  "status": "pending",
  "event": "push",
  "action": "",
  "link": "https://git.fsfe.org/fsfe-system-hackers/lychee/compare/0000000000000000000000000000000000000000...863bb2ef7decef26357957049223686d835c4b1d",
  "timestamp": 0,
  "message": "Add renovate.json\n",
  "before": "0000000000000000000000000000000000000000",
  "after": "863bb2ef7decef26357957049223686d835c4b1d",
  "ref": "refs/heads/renovate/configure",
  "source_repo": "",
  "source": "renovate/configure",
  "target": "renovate/configure",
  "author_login": "renovate-bot",
  "author_name": "Renovate Bot",
  "author_email": "renovate-bot@fsfe.org",
  "author_avatar": "https://git.fsfe.org/user/avatar/renovate-bot/-1",
  "sender": "renovate-bot",
  "started": 0,
  "finished": 0,
  "created": 1646210027,
  "updated": 1646210027,
  "version": 1
}

// POST https://drone.fsfe.org/api/repos/fsfe-system-hackers/lychee/builds/39
// HTTP/1.1 200 OK
// Server: nginx/1.18.0
// Date: Wed, 02 Mar 2022 08:33:47 GMT
// Content-Type: application/json
// Content-Length: 786
// Connection: keep-alive
// Cache-Control: no-cache, no-store, must-revalidate, private, max-age=0
// Expires: Thu, 01 Jan 1970 00:00:00 UTC
// Pragma: no-cache
// Vary: Origin
// Strict-Transport-Security: max-age=63072000
// Request duration: 0.453213s
#+END_SRC

#+begin_src restclient
GET https://drone.fsfe.org/api/repos/fsfe-system-hackers/lychee
Authorization: Bearer ***
#+end_src

#+RESULTS:
#+BEGIN_SRC js
{
  "id": 10084,
  "uid": "711",
  "user_id": 2,
  "namespace": "fsfe-system-hackers",
  "name": "lychee",
  "slug": "fsfe-system-hackers/lychee",
  "scm": "",
  "git_http_url": "https://git.fsfe.org/fsfe-system-hackers/lychee.git",
  "git_ssh_url": "git@git.fsfe.org:fsfe-system-hackers/lychee.git",
  "link": "https://git.fsfe.org/fsfe-system-hackers/lychee",
  "default_branch": "master",
  "private": false,
  "visibility": "public",
  "active": true,
  "config_path": ".drone.yml",
  "trusted": true,
  "protected": false,
  "ignore_forks": true,
  "ignore_pull_requests": false,
  "auto_cancel_pull_requests": false,
  "auto_cancel_pushes": false,
  "auto_cancel_running": false,
  "timeout": 60,
  "counter": 58,
  "synced": 1591277419,
  "created": 1591277419,
  "updated": 1599658219,
  "version": 62,
  "archived": false
}

// GET https://drone.fsfe.org/api/repos/fsfe-system-hackers/lychee
// HTTP/1.1 200 OK
// Server: nginx/1.18.0
// Date: Thu, 03 Mar 2022 16:27:58 GMT
// Content-Type: application/json
// Content-Length: 716
// Connection: keep-alive
// Cache-Control: no-cache, no-store, must-revalidate, private, max-age=0
// Expires: Thu, 01 Jan 1970 00:00:00 UTC
// Pragma: no-cache
// Vary: Origin
// Strict-Transport-Security: max-age=63072000
// Request duration: 0.116425s
#+END_SRC
